Processed Dataset
=================

ICAT Registration
-----------------

Register a processed dataset with ICAT

.. code-block:: bash

    icat-store-processed --beamline id00 \
        --proposal id002207 \
        --path /data/visitor/.../PROCESSED_DATA/integrated_A12 \
        --dataset integrated \
        --sample mysample \
        --raw /data/visitor/.../RAW_DATA/collectionA/collectionA_dataset1 \
        --raw /data/visitor/.../RAW_DATA/collectionA/collectionA_dataset2 \
        -p FIELD1=value1 \
        -p FIELD2=value2

.. warning:: At least one *--raw* dataset needs to be provided for ICAT to accept it.

The equivalent in *python* (metadata is optional)

.. code-block:: python

    from pyicat_plus.client.main import IcatClient
    from pyicat_plus.client import defaults

    client = IcatClient(metadata_urls=defaults.METADATA_BROKERS)

    metadata = {"FIELD1": "value1", "FIELD2": "value2"}

    client.store_processed_data(
        beamline="id00",
        proposal="id002207",
        dataset="integrated",
        path="/data/visitor/.../PROCESSED_DATA/integrated_A12",
        raw=["/data/visitor/.../RAW_DATA/collectionA/collectionA_dataset1",
             "/data/visitor/.../RAW_DATA/collectionA/collectionA_dataset2",]
        metadata={...}
    )

    client.disconnect()
