pyicat-plus |version|
=====================

*pyicat-plus* is a python client for ICAT+.

*pyicat-plus* has been developed by the `Software group <https://www.esrf.fr/Instrumentation/software>`_
of the `European Synchrotron <https://www.esrf.fr/>`_.

ICAT is essentially a database of investigations where each investigation is a timeslot allocated to a
proposal on a specific beamline. Each investigation contains a flat list of *datasets*. There are two types
of datasets:

* *raw*: data recorded and registered with ICAT by the acquisition control system
* *processed*: results associated to one of more raw datasets

A raw or processed dataset corresponds to a directory in the investigation directory structure following
the `recommendations <https://confluence.esrf.fr/display/DATAWGWK/Experiment+folders>`_ of the ESRF data working group.

.. toctree::
    :hidden:

    raw_data
    processed_data
    metadata
    api
