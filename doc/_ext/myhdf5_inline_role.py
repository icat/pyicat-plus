import re
import os
from docutils import nodes
from pyicat_plus.apps.icat_as_nexus import save_icat_as_nexus


def setup(app):
    app.add_role("myhdf5", myhdf5_role)
    app.connect("html-page-context", inject_dynamic_url_js)
    app.connect("config-inited", generate_icat_dataset_metadata)


def myhdf5_role(name, rawtext, text, lineno, inliner, options={}, content=[]):
    matches = re.match(r"(\S+)\s*<([^<>]+)>", text)
    display_text = matches.group(1)
    filename = matches.group(2)

    url_template = f"https://myhdf5.hdfgroup.org/view?url=placeholder{filename}"

    link = f'<a class="myhdf5" href="{url_template}">{display_text}</a>'

    node = nodes.raw("", link, format="html")
    return [node], []


def inject_dynamic_url_js(app, pagename, templatename, context, doctree):
    if app.builder.name != "html" or doctree is None:
        return

    script = """
    <script>
    document.addEventListener("DOMContentLoaded", function() {
        var links = document.querySelectorAll("a.myhdf5");
        var currentURL = window.location.href.replace("metadata.html", "");
        var resourceURL = encodeURIComponent(currentURL + "_static/");
        links.forEach(function(link) {
            var href = link.getAttribute("href");
            link.setAttribute("href", href.replace("placeholder", resourceURL));
        });
    });
    </script>
    """

    context["body"] += script


def generate_icat_dataset_metadata(app, config):
    output_filename = os.path.join(app.srcdir, "_static", "icat_dataset_metadata.h5")
    os.makedirs(os.path.dirname(output_filename), exist_ok=True)
    save_icat_as_nexus(output_filename)
