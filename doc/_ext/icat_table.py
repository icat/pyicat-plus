import re
from typing import List, Dict, Any

from docutils import nodes
from docutils.parsers.rst import Directive

from pyicat_plus.metadata.definitions import load_icat_fields


class InsertIcatTable(Directive):
    def run(self):
        rows = _generate_table_data()
        table_node = self._create_table(rows)
        search_box = self._create_search_box()
        script = self._create_filter_script()
        return [search_box, table_node, script]

    def _create_table(self, rows: List[Dict[str, Any]]) -> nodes.container:
        # Create a container for the scrollable table
        scrollable_container = nodes.container(classes=["scrollable-table"])

        # Create the table node
        table_node = nodes.table(classes=["table", "table-bordered", "table-striped"])
        table_node["ids"] = ["icat-table"]
        tgroup = nodes.tgroup(cols=len(rows[0]))
        table_node += tgroup

        # Create column specifications
        for _ in rows[0]:
            colspec = nodes.colspec(colwidth=1)
            tgroup += colspec

        # Create and add table header
        thead = nodes.thead()
        tgroup += thead
        header_row = nodes.row()
        for key in rows[0]:
            entry = nodes.entry()
            entry += nodes.paragraph(text=key)
            header_row += entry
        thead += header_row

        # Create and add table body
        tbody = nodes.tbody()
        tgroup += tbody
        for row_data in rows:
            row = nodes.row()
            row["classes"].append("table-row")
            for key, value in row_data.items():
                entry = nodes.entry()
                value = str(value)

                if re.search(r"https?://\S+", value):
                    parts = re.split(r"(https?://\S+)", value)
                    paragraph = nodes.paragraph()
                    for part in parts:
                        if re.match(r"https?://\S+", part):
                            url_node = nodes.raw(
                                text=f'<a href="{part}">{part}</a>', format="html"
                            )
                            paragraph += url_node
                        else:
                            paragraph += nodes.Text(part)
                    entry += paragraph
                else:
                    entry += nodes.paragraph(text=str(value))

                row += entry
            tbody += row

        # Add the table node to the scrollable container
        scrollable_container += table_node

        return scrollable_container

    def _create_search_box(self) -> nodes.raw:
        search_html = """
        <div>
            <input type="text" id="search-box" placeholder="Search...">
        </div>
        """
        return nodes.raw("", search_html, format="html")

    def _create_filter_script(self) -> nodes.raw:
        script_html = """
        <script>
            document.addEventListener("DOMContentLoaded", function() {
                var searchBox = document.getElementById("search-box");
                searchBox.addEventListener("input", function() {
                    var filter = searchBox.value.toLowerCase();
                    var rows = document.querySelectorAll(".table-row");
                    rows.forEach(function(row) {
                        var text = row.textContent.toLowerCase();
                        if (text.includes(filter)) {
                            row.style.display = "";
                        } else {
                            row.style.display = "none";
                        }
                    });
                });
            });
        </script>
        """
        return nodes.raw("", script_html, format="html")


def _generate_table_data() -> List[Dict[str, Any]]:
    icat_fields = load_icat_fields()
    rows = []

    for field in icat_fields.iter_fields():
        icat_name = field.field_name
        nexus_name = field.parent + field.name
        field_units = field.units or ""
        field_type = field.nxtype or ""
        field_description = field.description or ""
        row = {
            "ICAT": icat_name,
            "Description": field_description,
            "Type": field_type,
            "Units": field_units,
            "NeXus": nexus_name,
        }
        rows.append(row)

    return rows


def setup(app):
    app.add_directive("insert_icat_table", InsertIcatTable)
