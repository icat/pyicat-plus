Dataset Metadata
================

Each ICAT dataset can have metadata associated with it.

.. insert_icat_table::

Relation to NeXus
-----------------

To save all possible ICAT dataset metadata fields in a `NeXus-compliant <http://www.nexusformat.org/>`_ HDF5 file

.. code-block:: bash

    icat-nexus-definitions [--filename=icat.h5] [--url https://...]

By default the definitions from the locally installed `icat-esrf-definitions` are used
but a URL to the ICAT definitions XML file can be provided.

An example HDF5 file can be found :myhdf5:`here <icat_dataset_metadata.h5>`.
