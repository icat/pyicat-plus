import warnings

warnings.warn(f"{__name__} will be removed in the next release", DeprecationWarning)

Session = dict
