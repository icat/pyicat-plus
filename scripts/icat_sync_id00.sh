#!/bin/bash

# Update a local session cache for ID00
icat-sync-raw --beamline id00 --save-dir ./ --cache-dir ./ "$@"
