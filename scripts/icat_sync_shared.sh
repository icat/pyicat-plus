#!/bin/bash

# Update the shared session cache without printing the session summaries
icat-sync-raw --save-dir ./ --cache-dir /data/scisoft/icat_sync --no-print --invalidate-cache "$@"
