# CHANGELOG.md

## Unreleased

## 0.6.0

New features:

* `IcatClient.get_session_information` gets information about the session in ICAT.
* `IcatClient` constructor argument `icat_session_id` so login is not needed.
* `IcatClient.get_samples_by` get samples for a given investigation ID, instrument name or sample IDs.
* `IcatClient.get_investigations_by` no allows getting investigations by investigation IDs.

## 0.5.1

Bug fixes:

- `get_parcels_by` was using the wring query parameter (it mist be `investigationId`)
- Normalize paths when sending datasets to the ingester

## 0.5.0

Changes:

- Deprecate the helper clients in `IcatClient` that were exposed as public properties.
- Drop Python 3.6 and 3.7

New features:

- Support `NX_BOOLEAN` and `NX_INT` in the metadata XML definition.

Bug fixes:

- Remove leading and trailing spaces in the ICAT metadata field name in the XML definition.

## 0.4.1

Changes:

- remove unnecessary `numpy` version restriction

## 0.4.0

New features:

- provide authentication for the restricted part of the ICAT+ API
- reschedule investigations (allows adding data to experimental sessions from the past)
- retrieve a list of parcel information associated with an investigation
- retrieve a list of investigation information matching certain criteria

## 0.3.6

New features:

- Support `NX_BOOLEAN` and `NX_INT` in the metadata XML definition.

Bug fixes:

- Remove leading and trailing spaces in the ICAT metadata field name in the XML definition.

## 0.3.5

Bug fixes:

- fix `icat-sync-raw` invalidate session

## 0.3.4

Bug fixes:

- fix `icat-sync-raw` for python 3.8

## 0.3.3

Changes:

- `icat-sync-raw`: add `mx` format
- Improve documentation
- Add `machine` and `software` metadata information

## 0.3.2

Bug fixes:

- fix the `requires` section in `pyproject.toml`

## 0.3.1

Bug fixes:

- `test_icat_metadata_to_hdf5` test should not break when `icat-esrf-definitions` changes

## 0.3.0

Changes:

- `icat-sync-raw`: no saving by default instead of saving in the local directory
- elogbook messages: allow specifying mimetype, editable or not, formatted or not

## 0.2.1

Bug fixes:

- `icat-sync-raw`: skip `chmod` of files in `--savedir`

## 0.2.0

Changes:

- ICAT ESRF definitions fallback in a separate project
- Add documentation
- Improve investigation selection
- Replace local query pool code with `querypool` package

New features:

- API to synchronize raw data with ICAT
- API for adding/changing existing dataset metadata in ICAT
- API to add files to datasets that are registered without files

Bug fixes:

- Fix bug in `icat-store-raw --metadatafile`
- Fix printing of namespace object in the Bliss shell

## 0.1.10

Bug fixes:
  - Fix the setuptools version requirement
  
## 0.1.9

Bug fixes:

- Remove leading and trailing spaces in the ICAT metadata field name in the XML definition.

## 0.1.7

Bug fixes:

- Add missing dependency

## 0.1.6

Bug fixes:

- IcatInvestigationClient: investigation could not have a start date
- IcatClient: allow the "raw" argument for storing a dataset to be a string

## 0.1.5

Bug fixes:

- IcatClient: fix default e-logbook metadata (software, machine, ...)

## 0.1.4

Bug fixes:

- IcatMessagingClient: capture exception when disconnecting when already disconnected

## 0.1.3

Changes:

- Retry failing Stomp connections
- Icat default arguments in separate module

## 0.1.2

Changes:

- Update ICAT definitions

## 0.1.1

Bug fixes:

- mising test requirement pytest-timeout

## 0.1.0

New features:

- E-Logbook API
- API to register datasets (direct or through files)
